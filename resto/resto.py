#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function

###########################
# Imports
###########################

import json
from datetime import datetime, date, timedelta, time
import urllib
import os
import sys

import menuparser
import menubox
import menupage

###########################
# Configuration
###########################

# Settings for output format
menubox.PAD = 3
menubox.COWSAY = True
               
# Paths
data_file = 'etterbeek.en.json'
#basedir = ['/data/user/chris/resto/','/h'][socket.gethostname()=='simmel']
basedir = '/home/chris/Software/utils/resto/'
data_path = os.path.join(basedir, data_file) # has to be world-writable!

# Set the same paths for the parser, if needed
menuparser.SAVE_PATH = basedir

###########################
# Get weekly menu
###########################

def check_if_current_file(path,date_today):
    # file does not exist at all
    if not os.path.isfile(data_path):
        return False
    # file is empty
    if os.stat(data_path).st_size == 0:
        return False
    mtime = os.stat(data_path).st_mtime
    date_modif = datetime.fromtimestamp(mtime)
    delta_days = (date_today.date() - date_modif.date()).days
    # updated over a week ago
    if delta_days>6:
        return False
    weekday_today = date_today.weekday()
    weekday_modif = date_modif.weekday()
    # updated last week
    if (weekday_today < weekday_modif):
        return False    
    # updated on a weekday but it's weekend already
    if (weekday_today>4) and (weekday_modif<5):
        return False
    # all other cases
    return True


def main():
    datetime_today = datetime.now()
    date_today = datetime_today.date()
    if not check_if_current_file(data_path, datetime_today):
        print("Downloading...")
        menuparser.main()

    # Read JSON data
    try:
        with open(data_path) as f:
            data = json.load(f)
        assert data # has to be non-empty
    except:
        sys.exit('Could not load data from file {data_path}'.format(**locals()))
        
    ###########################
    # Find relevant day in data
    ###########################

    # Check which day to look for: today, tomorrow, next Monday
    closing_hour = {'etterbeek':time(14,00),
                    'jette':time(14,00),
                   }[data_file.split('.')[0]]
    past_closing = datetime_today.time()>=closing_hour
    on_weekend = (date_today.weekday() + int(past_closing))>4
    if on_weekend:
        days_to_next_monday = 7 - date_today.weekday()
        next_day = date_today + timedelta(days_to_next_monday)
    elif past_closing:
        next_day = date_today + timedelta(1)
    else:
        next_day = date_today

    # Find date in menu data
    def string_to_date(ds):
        y,m,d = tuple(map(int,ds.split('-')))
        return date(y,m,d)

    menu_dates = [string_to_date(m[u'date']) for m in data]
    next_to_first = (menu_dates[0] - next_day).days

    # Case 1) found it => display with matching name
    if next_day in menu_dates:
        # should be 0 or 1, but being safe in case they change it
        relevant_index = menu_dates.index(next_day)

    # Case 2) found a later => display next
    # assume they are time-sorted
    elif next_to_first>0:
        relevant_index = 0

    # Case 3) found only earlier or none at all => error
    else:
        sys.exit('Could not find next menu in data')
        
    ###########################
    # Output
    ###########################

    # Print output to terminal
    menubox.main(data[relevant_index], menu_dates[relevant_index], date_today)
    #menupage.main(data[relevant_index], menu_dates[relevant_index], date_today)

if __name__=='__main__':
    main()
