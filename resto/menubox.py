#!/usr/bin/env python3
# coding: utf-8
from __future__ import print_function

from datetime import datetime, date, timedelta
import calendar
import shutil, subprocess

# Colour names
CEND      = '\33[0m'
CBOLD     = '\33[1m'
CITALIC   = '\33[3m'
CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CCYAN2  = '\33[96m'
CWHITE2  = '\33[97m'

all_control_characters = [CEND,CBOLD,CITALIC,CGREY,CRED2,CGREEN2,
                          CYELLOW2,CBLUE2,CVIOLET2,CCYAN2,CWHITE2]

# Default settings
COWSAY=False
PAD = 3

# Formatting definitions
cowbody = """\
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\\
                ||----w |
                ||     ||"""
color_dict = {'Soup':CWHITE2,
              'Menu 1':CRED2,
              'Menu 2':CVIOLET2,
              'Soep':CWHITE2,
              'Fish':CBLUE2,
              'Pasta':CYELLOW2,
              'Veggie':CGREEN2,
              'Vis': CBLUE2,
              'Wok':CCYAN2}
header = ' %s{0}%s ({1}) '%(CBOLD,CEND)

# Assemble coloured strings with limited length
def assemble_body(menu, max_line_length=74):
    lines = []
    for m in menu:
        name = m[u'name']
        dish = m[u'dish']
        color_dict.setdefault(name,CWHITE2)
        dish_color = CBOLD+color_dict[name]
        name_string = '%6s: '%name
        name_string_color = dish_color + name_string + CEND
        # the dish name could be oversized, and the menu name can be >6 characters
        dish_truncated = dish[:(max_line_length-len(name_string))]
        lines.append(name_string_color + dish_truncated)
    return lines    
    
def date_description(date_i, date_0):
    delta_days = (date_i - date_0).days
    weekday_name = calendar.day_name[date_i.weekday()]
    if delta_days==0:
        return "Today, %s"%weekday_name
    elif delta_days==1:
        return "Tomorrow, %s"%weekday_name
    elif delta_days < 7:
        return weekday_name
    else:
        return "In {0} days".format(delta_days)

def assemble_header(date_string, date_longname):
    return header.format(date_longname,date_string)

# Print box
def remove_format(s):
    no_format = s
    for cc in all_control_characters:
        no_format = no_format.replace(cc,'')
    return no_format
def ljust(s,width,fillchar=' '):
    no_format = remove_format(s)
    aligned = no_format.ljust(width,fillchar)
    return aligned.replace(no_format,s)
def center(s,width,fillchar=' '):
    no_format = remove_format(s)
    aligned = no_format.center(width,fillchar)
    return aligned.replace(no_format,s)
def get_display_length(s):
    return len(remove_format(s))

def print_box(header, body, pad=3, cowsay=False):
    textwidth = max(map(get_display_length,[header]+[l for m in body for l in m]))
    boxwidth = textwidth + 2*pad
    header = center(' '+header+' ', boxwidth,u'─')
    separator = textwidth*u'─'
    
    # insert a separator after each menu of the day
    lines = [l+[separator] for l in body]
    # flatten the nested lost and remove the last separator
    lines = [l for m in lines for l in m][:-1]
    # add vertical padding
    lines = [''] + lines + ['']
    
    print(u'╭' + header + u'╮')
    for i,l in enumerate(lines):
        centered = center(ljust(l,textwidth),boxwidth)
        print (u'│'+centered+u'│')
    print (u'╰' + boxwidth*u'─'+u'╯')
    
    if cowsay:
        print (cowbody)

def get_terminal_width(default=80):
    # Works for Python3
    try:
        return shutil.get_terminal_size().columns
    except:
        pass
    # Works on Linux
    try:
        return int(subprocess.check_output(['stty', 'size']).split()[1])
    except:
        pass
    # Fallback: default value
    return default

# Main
def main(data, date_i, date_today):
    header_line = assemble_header(data[u'date'], date_description(date_i, date_today))
    line_length = get_terminal_width() - 2*(PAD+1)
    lines = [assemble_body(menu, max_line_length=line_length)
             for menu in data[u'menus']]
    print_box(header_line, lines, pad=PAD, cowsay=COWSAY)


