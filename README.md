## CURRENTLY BROKEN

Since this script was last updated, the VUB restaurant menus have moved to entirely different websites, with an entirely different structure. Requesting and parsing will need to be completely overhauled. I recommend the `beautifulsoup4` package for anyone brave enough to try.

## python-vub-resto...

...is a CLI tool to download, parse, and display the menu of (any of) the VUB restaurant(s). It's written in Python. The parser `menuparser.py` is based on my fork of Christoph de Troyer's scraper. It stores the parsed data in a JSON file, and for speed it does not try to regenerate this unless necessary.
The main script `resto.py` tries to find out whether the JSON file is still current, which day's menu to display, and then hands the information to `menubox.py` which print a nicely aligned and titled table of the menu, inside a box, with optional cowsay.

## Python

Tested both with Python 2.7 and Python >3.4, if it breaks for any of them let me know or feel free to make a pull request.

## Install

 - `pip install lxml`
 - `pip install cssselect`
 - `pip install requests`
 
## To do
 - Instead of just printing menu, open a curses screen that lets you switch between days with the left- and right-arrow keys.
 - Make configuration more transparent and user-friendly, i.e.
 - - replace hard-coded config with parsed arguments
 - - optionally, add tool to store defaults in config file
 - - have one single point where the config is transmitted to the parser, box
 - Use classes instead of modules?

 
